package com.mastercard.commercial.ta.mergemonitor.mergemonitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MergemonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MergemonitorApplication.class, args);
	}

}

